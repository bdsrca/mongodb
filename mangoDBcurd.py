from pymongo import MongoClient
import pprint               #pretty print for jason

#pip install pymongo
client = MongoClient("localhost",27017)
#find {borough:"Manhattan",cuisine:"American"}
mydata = client.restaurant_db.restaurant
myQ    = mydata.find({"borough":"Manhattan","cuisine":"American"})
print(myQ.count())  # count
pprint.pprint(myQ.next())

# new_posts = [{"author": "Mike", "text": "Another post!", "tags": ["bulk", "insert"], "date": 2017},
#  {"author": "Eliot",
#  "title": "MongoDB is fun",
#  "text": "and pretty easy too!",
#  "date": 2012}]
# collection.insert_many(new_posts)


for post in mydata.find():
    pprint.pprint(post,'\n\n')

for post in mydata.find({"author": "Hassan"}):
    pprint.pprint(post, '\n\n')

print(mydata.find({"author": "Mike"}).count())

# Using the restaurant collection already loaded in the Mongo cluster, write a script to count all the
# restaurants located in the Brooklyn borough.
db = client['restaurant_db']
restaurant = db.restaurant
print('borough from Brooklyn:', restaurant.find({"borough": "Brooklyn"}).count())

print('borough from Brooklyn and cuisine american:', restaurant.find( { "$and" : [{"cuisine" :
"American"}, {"borough": "Brooklyn"}]}).count())

mydata.index_infomation()