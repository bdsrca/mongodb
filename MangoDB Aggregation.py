from pymongo import MongoClient
from IPython.display import clear_output
import pprint               #pretty print for jason

#pip install pymongo
client = MongoClient("localhost",27017)
#find {borough:"Manhattan",cuisine:"American"}
mydata = client.restaurant_db.restaurant
d      = mydata.aggregate([{"$match":{"borough":"Manhattan"}}])
d.next()
#grouping
d2      = mydata.aggregate([{"$match":{"borough":"Manhattan"}},
                            {"$group": {"_id":"$cuisine","count": {"$sum":1}}},
                            {"$limit":100},
                            {"$sort":{"count":-1}}                  #desc -1 asscending 1
                            ])
#help(d2)
##pprint.pprint(d2.__dict__)
##d2d = d2.__dict__["_CommandCursor__data"]
clear_output()
#option 1 print a list of result
##pprint.pprint(list(d2))

#option 2 print by a loop
for doc in d2:
    print(doc)

