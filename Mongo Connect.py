from pymongo import MongoClient
#pip install pymongo
client = MongoClient("localhost",27017)
db = client["test123"]          # name of database
db.collection1
db.collection1.insert_one({"aa":"1"})
#help(db.collection1.drop)
#dir(db.collection1)    # list all commands

#This is 4 transactions
for item in [11,23,45,12]:
    db.collection1.insert({"order":item})

#This is 1 transactions in one shot
List = []
for item in [1,2,3,4]:
    List.append({"Good order":item})
db.collection1.insert(List)

db.collection1.update({"Good Order":1},{"updated":True})
print(db.collection1.update({"Good order":1},{"updated":True}))

Result = db.collection1.remove({"updated":True})
print(Result)

print(db.collection1.find_one())
